from osgeo import gdal
import matplotlib.pyplot as plt
import numpy as np
import gdal


def recup_nuages(array):

    # Nuages = 237

    I, J = np.shape(array)
    nouvelle = np.zeros([I, J])
    for i in range(I):
        for j in range(J):
            if array[i, j] == 250:
                nouvelle[i, j] = 1
    return nouvelle


def creer_masque(nuage1, nuage2):
    """
    Takes two binary images of the clouds for two differents days
    Create a mask containing the position of the useful information of nuage 2 that is not contained in nuage 1
    Input :
        nuage1, nuage2 : Binary arrays
    Output :
        masque : binary array with 1 on the usefull part, 0 elsewhere
    """
    masque = nuage1 + nuage2
    masque *= nuage1

    I, J = np.shape(masque)
    invmasque = np.zeros([I, J])
    for i in range(I):
        for j in range(J):
            if masque[i, j] == 2 or masque[i, j] == 0:
                masque[i, j] = 0
                invmasque[i, j] = 1

    print("Masque")
    plt.figure(3)
    plt.imshow(masque, cmap="binary", interpolation=None)
    return masque, invmasque


def combinaison_nuages(image1, image2):
    nuage1 = recup_nuages(image1)
    nuage2 = recup_nuages(image2)

    masque, invmasque = creer_masque(nuage1, nuage2)
    res = invmasque * image1 + masque * image2
    return res


def create_array_from_TIFF(file_path):
    """
    Converts a TIFF image with only one band to a Numpy array, making it easy to plot and to manipulate

    Input :
        file_path : The path of the TIFF image

    Output :
        im_array : An Numpy array of the TIFF image
    """

    dataset = gdal.Open(file_path)
    # if dataset.RasterCount > 1 :
    #   print('To many informations in the file, cannot decide which to consider')
    #   return None

    band = dataset.GetRasterBand(1)
    im_array = band.ReadAsArray()

    return im_array


def clouds_substracting(list_of_file_path):
    """
    Compute the total of visible pixels from mutliples clouds TIFF images.

    Input :
        list_of_file_path : A list containing the different path of all the TIFF images.

    Output :
        res : A binary Numpy array of the clouds distribution
    """

    first = create_array_from_TIFF(list_of_file_path[0])
    I, J = np.shape(first)
    res = np.ones([I, J])

    for file_path in list_of_file_path:
        new = create_array_from_TIFF(file_path)
        for i in range(I):
            for j in range(J):
                # Les pixels inexistants
                if np.isnan(new[i, j]):
                    res[i, j] = 1
                else:
                    res[i, j] *= new[i, j]
    print("New proportion = ", clouds_proportion(res), " % de nuages")
    return res


def clouds_proportion(im_array):
    """
    Compute the proportion of clouds on an image

    Input :
        im_array : A binary Numpy array, the image analysed

    Output :
        res : the proportion of clouds
    """
    somme = 0
    (I, J) = np.shape(im_array)
    for i in range(I):
        for j in range(J):
            somme += im_array[i, j]
    res = 100 * somme / (I * J)
    return res


def plot_nuages(im_array):
    """
    Plot a binary array in black and white

    Input :
        im_array : A Numpy array of the binary image that will be plot

    Output :
        None
    """

    plt.figure(1, facecolor=[0, 0, 0], edgecolor=[0, 0, 0], tight_layout=True)
    plt.axis("off")
    plt.imshow(im_array, cmap="binary", interpolation=None)
    plt.show()
