from skimage import io
import code_function as cf

##################################################################
"""Variables declaration"""
##################################################################


Choise = [None, "water", "snow", "cloud"]
Restart = "1"
data = []
treatedwater = []
thresholdedsnow = []
cloud = []
Big_Data = [data, treatedwater, thresholdedsnow, cloud]
path = ""
detect = False
path_saveTC = "C:/Users/louis/Documents/Ense3/teledection/saveTC/"
path_data = "C:/Users/louis/Documents/Ense3/teledection/data/"
##################################################################

while Restart == "1":
    C = 0
    print("\nwhat we do :")
    try:
        C = int(
            input(
                "\n1: select path \n2: detection \nnext option require detection \n3: count % \n4: show image \n5: save as \n\nChoice : "
            )
        )
    except:
        cf.warning()

    # 1 : select path source
    if C == 1:
        path = cf.select_path()[0]

    # 2 : detection
    if C == 2:
        if path == "":
            path = cf.select_path()[0]

        detec = True
        Big_Data[0] = io.imread(path)
        print("data readed")

        print("processing clouds")
        Big_Data[3] = cf.clouds(Big_Data[0])
        print(
            "The pourcentage of",
            Choise[3],
            ":",
            cf.compter_pourcentage(Big_Data[3]),
            "%",
        )
        print("cloud done")

        c2 = 0
        c2 = input("enter 1 to continue process : ")

        if c2 == "1":
            print("processing water")
            Big_Data[1] = cf.traitementeau(Big_Data[0], 0, 1)
            print("water done")

            print("processing snow")
            Big_Data[2] = cf.seuil(cf.traitementneige(Big_Data[0], 0, 2), 0.6)
            cf.deletewater(Big_Data[2], Big_Data[1])
            print("snow done")
        print("\nProcess complete")

    # 3 : count %
    if C == 3 and detect:
        c3 = 0
        c3 = cf.select_option()
        print(
            "\nthe pourcentage of",
            Choise[c3],
            "is:",
            cf.compter_pourcentage(Big_Data[c3]),
            "%",
        )

    # 4 : show image
    if C == 4 and detect:
        c4 = 0
        c4 = cf.select_option()
        io.imshow(Big_Data[c4])
        io.show()

    # 5 : save
    if C == 5 and detect:
        c5_1 = 0
        c5_2 = "default.png"
        print("what we save ?")
        c5_1 = cf.select_option()
        print("where we save ?")
        t = False
        while not t:
            try:
                c5_2 = input("path/name : ")
                io.imsave(c5_2, Big_Data[c5_1])
                t = True
            except:
                cf.warning()
                t = False

    Restart = input("\ntype 1 to continue : ")
