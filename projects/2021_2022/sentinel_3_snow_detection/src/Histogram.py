from skimage import io
import numpy as np
import matplotlib.pyplot as plt
import code_function as cf

##################################################################
"""Variables declaration"""
##################################################################
data = []
treatedwater = []
thresholdedsnow = []
cloud = []
Big_Data = [data, treatedwater, thresholdedsnow, cloud]
disc = 100
path = "C:/Users/louis/Documents/Ense3/teledection/data/18_06_20.tiff"
##################################################################
# this code allows to make an histogram of pixel's value repartition
# which allows us to make a way more effective tresholding

Big_Data[0] = io.imread( path )
Big_Data[2] = cf.traitementneige( Big_Data[0], 0, 2 )
M = Big_Data[2].max()
m = Big_Data[2].min()
pas = np.linspace(m, M, disc)
hist = [0 for i in range(disc)]
for x in Big_Data[2] :
	for y in x :
		mm = abs( pas[0] - y )
		ind = 0
		for i in range(disc) :
			t = abs( pas[i] - y )
			if t < mm :
				mm = t
				ind = i
		hist[ind] += 1

io.imshow(Big_Data[2])
io.show()
plt.plot(pas, hist)
plt.show()

input('')